package ua.nure.itroi.main;

public class TransformerDemo {

    public static void main(String[] args) throws Exception {
        XSLTransform.main(new String[] {
                "./src/main/java/ua/nure/itroi/xml/location.xsl",
                "./src/main/java/ua/nure/itroi/xml/location.xsd.xml",
                "./src/main/java/ua/nure/itroi/xml/location.xsd.html"});
    }
}
