<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tns="http://it.nure.ua/xml/entity/location"
                xmlns:dt="http://it.nure.ua/xml/entity/dataTypes"
                xmlns:pl="http://it.nure.ua/xml/entity/place">

    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Локації</title>
                <style>
                    body { font-family: Arial, sans-serif; }
                    h1 { font-size: 24px; font-weight: bold; text-align: center; }
                    h2 { font-size: 20px; font-weight: bold; margin-top: 20px; }
                    p { font-size: 16px; }
                    table { width: 100%; border-collapse: collapse; margin-top: 10px; }
                    th, td { border: 1px solid black; padding: 8px; text-align: left; }
                    th { background-color: #f2f2f2; }
                </style>
            </head>
            <body>
                <h1>Локації</h1>
                <xsl:apply-templates select="tns:Location"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="tns:Location">
        <h2><xsl:value-of select="tns:LocationName"/></h2>
        <p>Координати:</p>
        <ul>
            <xsl:for-each select="tns:Area/tns:Coordinate">
                <li>Широта: <xsl:value-of select="dt:Latitude"/>, Довгота: <xsl:value-of select="dt:Longitude"/></li>
            </xsl:for-each>
        </ul>
        <p>Місця:</p>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Назва</th>
                    <th>Опис</th>
                    <th>Тип</th>
                    <th>Адреса</th>
                    <th>Координати</th>
                    <th>Час відвідування</th>
                    <th>Вхідний збір</th>
                    <th>Фото</th>
                    <th>Сезонність</th>
                    <th>Теги</th>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates select="tns:Places/tns:Place[pl:EntranceFee/dt:Price > 0]"/>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="tns:Place">
        <tr>
            <td><xsl:value-of select="pl:ID"/></td>
            <td><xsl:value-of select="pl:Name"/></td>
            <td><xsl:value-of select="pl:Description"/></td>
            <td><xsl:value-of select="pl:Type"/></td>
            <td>
                <xsl:value-of select="pl:Address/dt:Street"/>&#160;<xsl:value-of select="pl:Address/dt:HouseNumber/dt:Number | pl:Address/dt:HouseNumber/dt:NumberWithLetter"/>
                <xsl:if test="pl:Address/dt:Street and (pl:Address/dt:HouseNumber/dt:Number or pl:Address/dt:HouseNumber/dt:NumberWithLetter)">, </xsl:if>
                <xsl:value-of select="pl:Address/dt:City"/>
            </td>
            <td>Широта: <xsl:value-of select="pl:Coordinate/dt:Latitude"/>, Довгота: <xsl:value-of select="pl:Coordinate/dt:Longitude"/></td>
            <td><xsl:value-of select="pl:VisitTime"/></td>
            <td><xsl:value-of select="pl:EntranceFee/dt:Price"/>&#160;<xsl:value-of select="pl:EntranceFee/dt:Currency"/></td>
            <td>
                <xsl:for-each select="pl:Photos/pl:Photo">
                    <img src="{.}" alt="Фото місця" width="100" style="margin-right: 10px;"/>
                </xsl:for-each>
            </td>
            <td><xsl:value-of select="pl:Seasonality"/></td>
            <td>
                <ul>
                    <xsl:for-each select="pl:Tags/pl:Tag">
                        <li><xsl:value-of select="."/></li>
                    </xsl:for-each>
                </ul>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
